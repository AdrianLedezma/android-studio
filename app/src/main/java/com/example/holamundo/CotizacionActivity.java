package com.example.holamundo;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class CotizacionActivity extends AppCompatActivity {
    private TextView lblNombre, lblFolio, lblPagoInicial, lblPagoMensual;
    private EditText txtDescripcion, txtValorAuto, txtPorcentaje;
    private RadioButton rdb12, rdb18, rdb24, rdb36, rdb48;
    private Button btnCalcular, btnLimpiar, btnCerrar;
    private Cotizacion cotizacion;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_cotizacion);
        initComponents();
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });

        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String descripcion = txtDescripcion.getText().toString();
                String porcentajeStr = txtPorcentaje.getText().toString();
                String valorAutoStr = txtValorAuto.getText().toString();

                if (descripcion.isEmpty() || porcentajeStr.isEmpty() || valorAutoStr.isEmpty()) {
                    Toast.makeText(CotizacionActivity.this, "Faltaron datos por ingresar", Toast.LENGTH_SHORT).show();
                } else {
                    try {
                        float porcentaje = Float.parseFloat(porcentajeStr);
                        float valorAuto = Float.parseFloat(valorAutoStr);

                        if (porcentaje < 0 || porcentaje > 100) {
                            Toast.makeText(CotizacionActivity.this, "Ingrese un porcentaje valido", Toast.LENGTH_SHORT).show();
                        } else {

                            int plazo = 0;
                            if (rdb12.isChecked()) {
                                plazo = 12;
                            } else if (rdb24.isChecked()) {
                                plazo = 24;
                            } else if (rdb36.isChecked()) {
                                plazo = 36;
                            } else if (rdb48.isChecked()) {
                                plazo = 48;
                            }
                            cotizacion.setDescripcion(descripcion);
                            cotizacion.setValorAuto(valorAuto);
                            cotizacion.setPorEnganche(porcentaje);
                            cotizacion.setPlazos(plazo);

                            float enganche = cotizacion.calcularPagoInicial();
                            float pagoMensual = cotizacion.calcularPagoMensual();

                            String engancheF = String.format("%.2f", enganche);
                            String pagoMensualF = String.format("%.2f", pagoMensual);

                            lblPagoInicial.setText("Pago Inicial: $" + engancheF);
                            lblPagoMensual.setText("Pago Mensual: $" + pagoMensualF);

                            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                        }
                    } catch (NumberFormatException e) {
                        Toast.makeText(CotizacionActivity.this, "Por favor, ingrese valores numéricos válidos", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtDescripcion.setText("");
                txtValorAuto.setText("");
                txtPorcentaje.setText("");
                lblPagoInicial.setText("Pago Inicial:");
                lblPagoMensual.setText("Pago Mensual:");
                rdb12.setChecked(true);            }
        });
        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void initComponents(){
        lblNombre = findViewById(R.id.lblUsuario);
        lblFolio = findViewById(R.id.lblFolio);
        lblPagoInicial= findViewById(R.id.lblPagoInicial);
        lblPagoMensual = findViewById(R.id.lblPagoMensual);
        txtDescripcion = findViewById(R.id.txtDescripcion);
        txtValorAuto = findViewById(R.id.txtValor);
        txtPorcentaje = findViewById(R.id.txtPorPagoInicial);
        rdb12 = findViewById(R.id.rdb12);
        rdb24 = findViewById(R.id.rdb24);
        rdb36 = findViewById(R.id.rdb36);
        rdb48 = findViewById(R.id.rdb48);
        btnCalcular = findViewById(R.id.btnCalcular);
        btnCerrar =  findViewById(R.id.btnCerrar);
        btnLimpiar =  findViewById(R.id.btnLimpiar);
        cotizacion = new Cotizacion();
        lblFolio.setText("Folio: " + String.valueOf(cotizacion.generarId()));
        Bundle datos = getIntent().getExtras();
        String nombre = datos.getString("cliente");
        lblNombre.setText("Usuario: " + nombre);
        rdb12.setSelected(true);
    }
}