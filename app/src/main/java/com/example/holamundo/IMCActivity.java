package com.example.holamundo;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class IMCActivity extends AppCompatActivity {
    private TextView lblResultado;
    private EditText txtAltura, txtPeso;
    private Button btnCerrar, btnLimpiar, btnCalcular;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_imcactivity);
        iniciarComponentes();
        //Codificar los eventos click

        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // calcularIMC();
            }
        });

        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtAltura.setText("");
                txtPeso.setText("");
                lblResultado.setText("IMC es: ");
                lblResultado.setTextColor(getColor(R.color.black));

            }
        });

        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 calcularIMC();
            }
        });
    }

    public void iniciarComponentes() {
        lblResultado = findViewById(R.id.lblResultado);
        txtAltura = findViewById(R.id.txtAltura);
        txtPeso = findViewById(R.id.txtPeso);
        btnCerrar = findViewById(R.id.btnCerrar);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnCalcular = findViewById(R.id.btnCalcular);
    }
    @SuppressLint("ResourceAsColor")
    public void calcularIMC() {

        String alturaStr = txtAltura.getText().toString();
        String pesoStr = txtPeso.getText().toString();

        if (TextUtils.isEmpty(alturaStr) || TextUtils.isEmpty(pesoStr)) {
            Toast.makeText(IMCActivity.this, "Falto agregar el dato.", Toast.LENGTH_SHORT).show();
            return;
        } else{
            float altura = Float.parseFloat(alturaStr);
            float peso = Float.parseFloat(pesoStr);
            float imc = peso/(altura*altura);
            String imcF = String.format("%.2f", imc);
            lblResultado.setText("IMC: " + imcF);
            if(imc < 18.5){
                lblResultado.setTextColor(getColor(R.color.bajo));
            }else if(imc >= 18.5 && imc < 25){
                lblResultado.setTextColor(getColor(R.color.normal));
            }else if(imc >= 25 && imc < 30){
                lblResultado.setTextColor(getColor(R.color.sobrepeso));
            }else if(imc >=30){
                lblResultado.setTextColor(getColor(R.color.obeso));
            }
        }
    }
}