package com.example.holamundo;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;

public class cambioActivity extends AppCompatActivity {

    private EditText txtCantidad;
    private Spinner spinnerMonedas;
    private TextView txtTotal;
    private Button btnCalcular, btnLimpiar, btnCerrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cambio);

        iniciarComponentes();

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.monedas_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerMonedas.setAdapter(adapter);

        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calcularConversion();
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtCantidad.setText("");
                txtTotal.setText("Total $0");
                spinnerMonedas.setSelection(0);
            }
        });

        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void calcularConversion() {
        String cantidadStr = txtCantidad.getText().toString();
        if (cantidadStr.isEmpty()) {
            Toast.makeText(this, "Por favor, ingresa una cantidad", Toast.LENGTH_SHORT).show();
            return;
        }

        double cantidad = Double.parseDouble(cantidadStr);
        String monedaSeleccionada = spinnerMonedas.getSelectedItem().toString();
        double resultado = 0;

        switch (monedaSeleccionada) {
            case "Dólares Americanos":
                resultado = cantidad * 0.060136629;
                break;
            case "Euros":
                resultado = cantidad * 0.055383866;
                break;
            case "Dólar Canadiense":
                resultado = cantidad * 0.082071135;
                break;
            case "Libra Esterlina":
                resultado = cantidad * 0.047286191;
                break;
        }

        String resultadoFormateado = String.format("%.2f", resultado);
        txtTotal.setText("Total $" + resultadoFormateado);
    }

    public void iniciarComponentes(){
        txtCantidad = findViewById(R.id.txtCantidad);
        spinnerMonedas = findViewById(R.id.spinnerMonedas);
        txtTotal = findViewById(R.id.txtTotal);
        btnCalcular = findViewById(R.id.btnCalcular);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnCerrar = findViewById(R.id.btnCerrar);
    }

}
