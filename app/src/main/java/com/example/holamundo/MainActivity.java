package com.example.holamundo;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
    private TextView lblSaludo;
    private EditText txtSaludo;
    private Button btnSaludo, btnCerrar, btnLimpiar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lblSaludo = (TextView) findViewById(R.id.lblSaludo);
        txtSaludo = (EditText) findViewById(R.id.txtNombre);
        btnSaludo = (Button) findViewById(R.id.btnPulsame);
        btnCerrar = (Button) findViewById(R.id.btnCerrar);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);

        //Codificar evento click de limpiar
        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtSaludo.setText("");
                lblSaludo.setText("");
            }
        });
        //Codificar evento click de Cerrar
        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        //Codificar evento click de Saludar
        btnSaludo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txtSaludo.getText().toString().matches("")){
                    Toast.makeText(MainActivity.this, "Falto agregar informacion", Toast.LENGTH_SHORT).show();
                }else lblSaludo.setText("!Hola, " + txtSaludo.getText().toString() + "!");
            }
        });
    }
}